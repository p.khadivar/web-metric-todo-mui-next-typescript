import AddTodo from "@/components/add-todo";
import { Todos } from "@/components/todos";
import Navbar from "@/components/navbar";
import "./globals.css";
import { RiTodoLine } from "react-icons/ri";
import { Box, Grid, Typography } from "@mui/material";
import EventNoteIcon from '@mui/icons-material/EventNote';
// thapa technical SUBSCRIBE
const Page = () => {
  return (
    <Box>
      <Grid sx={{ display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center", marginTop: 10 }}>
        <Typography variant="h3" sx={{ marginBottom: 7 }}><EventNoteIcon fontSize="large"/> TODO NEXT + TYPESCRIPT <EventNoteIcon fontSize="large"/> </Typography>
        <Navbar />
        <AddTodo />
        <Todos />
      </Grid>
    </Box>
  );
};

export default Page;