"use client"
import { useTodos } from "@/store/todos";
import ListItem from '@mui/material/ListItem';
import Checkbox from '@mui/material/Checkbox';
import { Button, Typography } from "@mui/material";
import { useSearchParams } from "next/navigation";

export const Todos = () => {
    const { todos, toggleTodoAsCompleted, handleDeleteTodo } = useTodos();

    const searchParams = useSearchParams();
    const todosFilter = searchParams.get('todos')
    console.log("params " + todosFilter)

    let filteredTodos = todos;

    if (todosFilter === "active") {
        filteredTodos = todos.filter((todo) => !todo.completed);
    } else if (todosFilter === "completed") {
        filteredTodos = todos.filter((todo) => todo.completed);
    }

    // thapa technical SUBSCRIBE

    return (
        <ul className="main-task">
            {
                filteredTodos.map((todo) => {
                    return <li key={todo.id}>

                        {/*Assigns a unique ID to the checkbox. The ID is created by concatenating the string "todo-" with the id property of the todo object.*/}
                        <Checkbox id={`todo-${todo.id}`} checked={todo.completed} onChange={() => {
                            console.log(todo.completed)
                            toggleTodoAsCompleted(todo.id)
                        }
                        } />

                        <Typography variant="h5" > {todo.task}</Typography>
                        <Typography variant="body1">{todo.description}</Typography>

                        {
                            todo.completed && (
                                <Button variant="contained" color="error" type="button" onClick={() => handleDeleteTodo(todo.id)}>
                                    Delete
                                </Button>
                            )
                        }

                    </li>
                })
            }
        </ul>
    )
}