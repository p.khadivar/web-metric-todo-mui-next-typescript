// const AddTodo : any () => < h2> I am add todo  </h2>
'use client'
import Button from '@mui/material/Button';
import Grid from "@mui/material/Grid"
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import { FormEvent, useState } from "react";
import { useTodos } from "@/store/todos";
import { Box } from '@mui/material';

export function AddTodo() {
    const [todo, setTodo] = useState("");
    const [description, setDescription] = useState("")

    const { handleAddTodo } = useTodos();

    // thapa technical SUBSCRIBE

    function handleFormSubmit(e: FormEvent<HTMLFormElement>) {
        e.preventDefault();
        handleAddTodo(todo, description) // to add the data in an array
        setTodo("");
        setDescription("")
    }

    return (
        <form onSubmit={handleFormSubmit}>
            <Box sx={{ my: 4, display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>

                <TextField type="text" label="Title" value={todo}
                    onChange={(e) => setTodo(e.target.value)} />
                <TextField
                    sx={{ mx: "6rem" }}
                    id="outlined-multiline-static"
                    label="Description"
                    multiline
                    rows={2}
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                />
                <Button type="submit" variant="contained" size="large" color="primary">ADD</Button>
            </Box>
        </form >
    )
}

export default AddTodo;
